"use strict";

function turnPage (pageNum) {
    return function () {

        var nextPage = document.getElementById("page" + (pageNum + 1));

        nextPage.style.zIndex = 1;
    }
}

function load(){
    var pages = document.getElementsByClassName("page");


    for (var pageNum = 0; pageNum < pages.length; pageNum++){
        console.log(pageNum);
        var page = pages[pageNum];

        page.addEventListener("click", function () {
            this.classList.add("pageAnimation");
        });
        page.addEventListener("animationend", turnPage(pageNum));

    }
}

// load();